/**
 * FXML Controller class
 *
 * @author Ken
 */
package com.kfcstandard.grovefxdemo.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

public class GrovePiFXDisplayController {
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;
    
    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    
    @FXML // fx:id="fenceDistance"
    private TextField fenceDistance; // Value injected by FXMLLoader

    @FXML // fx:id="proximitySphere"
    private Sphere proximitySphere; // Value injected by FXMLLoader

    @FXML
    void distanceChange(ActionEvent event) {
        for (int x = 0; x < 10; ++x) {
            System.out.println("In changeDistance");
        }
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert fenceDistance != null : "fx:id=\"fenceDistance\" was not injected: check your FXML file 'GrovePiFXDisplay.fxml'.";
        assert proximitySphere != null : "fx:id=\"proximitySphere\" was not injected: check your FXML file 'GrovePiFXDisplay.fxml'.";

        PhongMaterial phongMaterial = new PhongMaterial();
        phongMaterial.setDiffuseColor(Color.GREENYELLOW);
        proximitySphere.setMaterial(phongMaterial);
    }
    
    public void setReading(double reading) {
        fenceDistance.setText("" + reading);
    }
    
}
